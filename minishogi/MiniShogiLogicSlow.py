from .MiniShogiLogic import *
from .MiniShogiPiecesSlow import *


class BoardSlow(Board):
    def __init__(self):
        self.strgameboard = [["" for j in range(5)] for i in range(5)]
        self.objgameboard = [[None for j in range(5)] for i in range(5)]
        # self.movesNum = 0
        self.pieceDict = {"k": (King, 1), "g": (GGeneral, 1), "s": (SGeneral, 1), "b": (Bishop, 1), "r": (Rook, 1), "p": (
            Pawn, 1), "K": (King, -1), "G": (GGeneral, -1), "S": (SGeneral, -1), "B": (Bishop, -1), "R": (Rook, -1), "P": (Pawn, -1)}
        self.objplayer = Player(1, [])
        self.otherobjplayer = Player(-1, [])
        self.startPositions()

    def startPositions(self):
        # Initializes a gameboard holding Piece objects and a gameboard holding string representations of the pieces
        objgameboard = self.objgameboard
        strgameboard = self.strgameboard
        pieceOrder = [King, GGeneral, SGeneral, Bishop, Rook]
        strOrder = ["k", "g", "s", "b", "r"]
        for x in range(0, 5):
            objgameboard[x][0] = pieceOrder[x](
                1, str(pieceOrder[x].__name__), (x, 0), 1, False)
            strgameboard[x][0] = strOrder[x]
            objgameboard[4-x][4] = pieceOrder[x](-1, str(
                pieceOrder[x].__name__), (4-x, 4), -1, False)
            strgameboard[4-x][4] = strOrder[x].upper()
        objgameboard[0][1] = Pawn(1, "Pawn", (0, 1), 1, False)
        strgameboard[0][1] = "p"
        objgameboard[4][3] = Pawn(-1, "Pawn", (4, 3), -1, False)
        strgameboard[4][3] = "P"

    def getStrGameBoard(self, objgameboard):
        strgameboard = [["" for j in range(5)] for i in range(5)]

        # Normal pieces
        for i in range(0, 5):
            for j in range(0, 5):
                if objgameboard[i][j] != None:
                    if objgameboard[i][j].pieceType == "King":
                        if objgameboard[i][j].team == 1:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+k"
                            else:
                                strgameboard[i][j] = "k"
                        else:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+K"
                            else:
                                strgameboard[i][j] = "K"

                    if objgameboard[i][j].pieceType == "GGeneral":
                        if objgameboard[i][j].team == 1:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+g"
                            else:
                                strgameboard[i][j] = "g"
                        else:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+G"
                            else:
                                strgameboard[i][j] = "G"

                    if objgameboard[i][j].pieceType == "SGeneral":
                        if objgameboard[i][j].team == 1:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+s"
                            else:
                                strgameboard[i][j] = "s"
                        else:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+S"
                            else:
                                strgameboard[i][j] = "S"

                    if objgameboard[i][j].pieceType == "Bishop":
                        if objgameboard[i][j].team == 1:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+b"
                            else:
                                strgameboard[i][j] = "b"
                        else:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+B"
                            else:
                                strgameboard[i][j] = "B"

                    if objgameboard[i][j].pieceType == "Rook":
                        if objgameboard[i][j].team == 1:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+r"
                            else:
                                strgameboard[i][j] = "r"
                        else:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+R"
                            else:
                                strgameboard[i][j] = "R"

                    if objgameboard[i][j].pieceType == "Pawn":
                        if objgameboard[i][j].team == 1:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+p"
                            else:
                                strgameboard[i][j] = "p"
                        else:
                            if objgameboard[i][j].promoted:
                                strgameboard[i][j] = "+P"
                            else:
                                strgameboard[i][j] = "P"

        return strgameboard
