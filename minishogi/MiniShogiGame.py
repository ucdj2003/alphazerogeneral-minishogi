from __future__ import print_function
import sys
import logging
from Game import Game
from minishogi.MiniShogiLogic import *
import numpy as np

all_valid_move = ['move a1 b1', 'move a1 b1 promote', 'move a1 c1', 'move a1 c1 promote', 'move a1 d1', 'move a1 d1 promote', 'move a1 e1', 'move a1 e1 promote', 'move a1 a2', 'move a1 a2 promote', 'move a1 a3', 'move a1 a3 promote', 'move a1 a4', 'move a1 a4 promote', 'move a1 a5', 'move a1 a5 promote', 'move a1 b2', 'move a1 b2 promote', 'move a1 c3', 'move a1 c3 promote', 'move a1 d4', 'move a1 d4 promote', 'move a1 e5', 'move a1 e5 promote', 'move a2 b2', 'move a2 c2', 'move a2 d2', 'move a2 e2', 'move a2 a1', 'move a2 a1 promote', 'move a2 a3', 'move a2 a4', 'move a2 a5', 'move a2 a5 promote', 'move a2 b3', 'move a2 c4', 'move a2 d5', 'move a2 d5 promote', 'move a2 b1', 'move a2 b1 promote', 'move a3 b3', 'move a3 c3', 'move a3 d3', 'move a3 e3', 'move a3 a1', 'move a3 a1 promote', 'move a3 a2', 'move a3 a4', 'move a3 a5', 'move a3 a5 promote', 'move a3 b4', 'move a3 c5', 'move a3 c5 promote', 'move a3 b2', 'move a3 c1', 'move a3 c1 promote', 'move a4 b4', 'move a4 c4', 'move a4 d4', 'move a4 e4', 'move a4 a1', 'move a4 a1 promote', 'move a4 a2', 'move a4 a3', 'move a4 a5', 'move a4 a5 promote', 'move a4 b5', 'move a4 b5 promote', 'move a4 b3', 'move a4 c2', 'move a4 d1', 'move a4 d1 promote', 'move a5 b5', 'move a5 b5 promote', 'move a5 c5', 'move a5 c5 promote', 'move a5 d5', 'move a5 d5 promote', 'move a5 e5', 'move a5 e5 promote', 'move a5 a1', 'move a5 a1 promote', 'move a5 a2', 'move a5 a2 promote', 'move a5 a3', 'move a5 a3 promote', 'move a5 a4', 'move a5 a4 promote', 'move a5 b4', 'move a5 b4 promote', 'move a5 c3', 'move a5 c3 promote', 'move a5 d2', 'move a5 d2 promote', 'move a5 e1', 'move a5 e1 promote', 'move b1 a1', 'move b1 a1 promote', 'move b1 c1', 'move b1 c1 promote', 'move b1 d1', 'move b1 d1 promote', 'move b1 e1', 'move b1 e1 promote', 'move b1 b2', 'move b1 b2 promote', 'move b1 b3', 'move b1 b3 promote', 'move b1 b4', 'move b1 b4 promote', 'move b1 b5', 'move b1 b5 promote', 'move b1 c2', 'move b1 c2 promote', 'move b1 d3', 'move b1 d3 promote', 'move b1 e4', 'move b1 e4 promote', 'move b1 a2', 'move b1 a2 promote', 'move b2 a2', 'move b2 c2', 'move b2 d2', 'move b2 e2', 'move b2 b1', 'move b2 b1 promote', 'move b2 b3', 'move b2 b4', 'move b2 b5', 'move b2 b5 promote', 'move b2 c3', 'move b2 d4', 'move b2 e5', 'move b2 e5 promote', 'move b2 c1', 'move b2 c1 promote', 'move b2 a3', 'move b2 a1', 'move b2 a1 promote', 'move b3 a3', 'move b3 c3', 'move b3 d3', 'move b3 e3', 'move b3 b1', 'move b3 b1 promote', 'move b3 b2', 'move b3 b4', 'move b3 b5', 'move b3 b5 promote', 'move b3 c4', 'move b3 d5', 'move b3 d5 promote', 'move b3 c2', 'move b3 d1', 'move b3 d1 promote', 'move b3 a4', 'move b3 a2', 'move b4 a4', 'move b4 c4', 'move b4 d4', 'move b4 e4', 'move b4 b1', 'move b4 b1 promote', 'move b4 b2', 'move b4 b3', 'move b4 b5', 'move b4 b5 promote', 'move b4 c5', 'move b4 c5 promote', 'move b4 c3', 'move b4 d2', 'move b4 e1', 'move b4 e1 promote', 'move b4 a5', 'move b4 a5 promote', 'move b4 a3', 'move b5 a5', 'move b5 a5 promote', 'move b5 c5', 'move b5 c5 promote', 'move b5 d5', 'move b5 d5 promote', 'move b5 e5', 'move b5 e5 promote', 'move b5 b1', 'move b5 b1 promote', 'move b5 b2', 'move b5 b2 promote', 'move b5 b3', 'move b5 b3 promote', 'move b5 b4', 'move b5 b4 promote', 'move b5 c4', 'move b5 c4 promote', 'move b5 d3', 'move b5 d3 promote', 'move b5 e2', 'move b5 e2 promote', 'move b5 a4', 'move b5 a4 promote', 'move c1 a1', 'move c1 a1 promote', 'move c1 b1', 'move c1 b1 promote', 'move c1 d1', 'move c1 d1 promote', 'move c1 e1', 'move c1 e1 promote', 'move c1 c2', 'move c1 c2 promote', 'move c1 c3', 'move c1 c3 promote', 'move c1 c4', 'move c1 c4 promote', 'move c1 c5', 'move c1 c5 promote', 'move c1 d2', 'move c1 d2 promote', 'move c1 e3', 'move c1 e3 promote', 'move c1 b2', 'move c1 b2 promote', 'move c1 a3', 'move c1 a3 promote', 'move c2 a2', 'move c2 b2', 'move c2 d2', 'move c2 e2', 'move c2 c1', 'move c2 c1 promote', 'move c2 c3', 'move c2 c4', 'move c2 c5', 'move c2 c5 promote', 'move c2 d3', 'move c2 e4', 'move c2 d1', 'move c2 d1 promote', 'move c2 b3', 'move c2 a4', 'move c2 b1', 'move c2 b1 promote', 'move c3 a3', 'move c3 b3', 'move c3 d3', 'move c3 e3', 'move c3 c1', 'move c3 c1 promote', 'move c3 c2', 'move c3 c4', 'move c3 c5', 'move c3 c5 promote', 'move c3 d4', 'move c3 e5', 'move c3 e5 promote', 'move c3 d2', 'move c3 e1', 'move c3 e1 promote', 'move c3 b4', 'move c3 a5', 'move c3 a5 promote', 'move c3 b2', 'move c3 a1', 'move c3 a1 promote', 'move c4 a4', 'move c4 b4', 'move c4 d4', 'move c4 e4', 'move c4 c1', 'move c4 c1 promote', 'move c4 c2', 'move c4 c3', 'move c4 c5', 'move c4 c5 promote', 'move c4 d5', 'move c4 d5 promote', 'move c4 d3', 'move c4 e2', 'move c4 b5', 'move c4 b5 promote', 'move c4 b3', 'move c4 a2', 'move c5 a5', 'move c5 a5 promote', 'move c5 b5', 'move c5 b5 promote', 'move c5 d5', 'move c5 d5 promote', 'move c5 e5', 'move c5 e5 promote', 'move c5 c1', 'move c5 c1 promote', 'move c5 c2', 'move c5 c2 promote', 'move c5 c3', 'move c5 c3 promote', 'move c5 c4', 'move c5 c4 promote', 'move c5 d4', 'move c5 d4 promote', 'move c5 e3', 'move c5 e3 promote', 'move c5 b4', 'move c5 b4 promote', 'move c5 a3', 'move c5 a3 promote', 'move d1 a1', 'move d1 a1 promote', 'move d1 b1', 'move d1 b1 promote', 'move d1 c1', 'move d1 c1 promote', 'move d1 e1', 'move d1 e1 promote', 'move d1 d2', 'move d1 d2 promote', 'move d1 d3', 'move d1 d3 promote', 'move d1 d4', 'move d1 d4 promote', 'move d1 d5', 'move d1 d5 promote', 'move d1 e2', 'move d1 e2 promote', 'move d1 c2', 'move d1 c2 promote', 'move d1 b3', 'move d1 b3 promote', 'move d1 a4', 'move d1 a4 promote', 'move d2 a2', 'move d2 b2', 'move d2 c2', 'move d2 e2', 'move d2 d1', 'move d2 d1 promote', 'move d2 d3', 'move d2 d4', 'move d2 d5', 'move d2 d5 promote', 'move d2 e3', 'move d2 e1', 'move d2 e1 promote', 'move d2 c3',
                  'move d2 b4', 'move d2 a5', 'move d2 a5 promote', 'move d2 c1', 'move d2 c1 promote', 'move d3 a3', 'move d3 b3', 'move d3 c3', 'move d3 e3', 'move d3 d1', 'move d3 d1 promote', 'move d3 d2', 'move d3 d4', 'move d3 d5', 'move d3 d5 promote', 'move d3 e4', 'move d3 e2', 'move d3 c4', 'move d3 b5', 'move d3 b5 promote', 'move d3 c2', 'move d3 b1', 'move d3 b1 promote', 'move d4 a4', 'move d4 b4', 'move d4 c4', 'move d4 e4', 'move d4 d1', 'move d4 d1 promote', 'move d4 d2', 'move d4 d3', 'move d4 d5', 'move d4 d5 promote', 'move d4 e5', 'move d4 e5 promote', 'move d4 e3', 'move d4 c5', 'move d4 c5 promote', 'move d4 c3', 'move d4 b2', 'move d4 a1', 'move d4 a1 promote', 'move d5 a5', 'move d5 a5 promote', 'move d5 b5', 'move d5 b5 promote', 'move d5 c5', 'move d5 c5 promote', 'move d5 e5', 'move d5 e5 promote', 'move d5 d1', 'move d5 d1 promote', 'move d5 d2', 'move d5 d2 promote', 'move d5 d3', 'move d5 d3 promote', 'move d5 d4', 'move d5 d4 promote', 'move d5 e4', 'move d5 e4 promote', 'move d5 c4', 'move d5 c4 promote', 'move d5 b3', 'move d5 b3 promote', 'move d5 a2', 'move d5 a2 promote', 'move e1 a1', 'move e1 a1 promote', 'move e1 b1', 'move e1 b1 promote', 'move e1 c1', 'move e1 c1 promote', 'move e1 d1', 'move e1 d1 promote', 'move e1 e2', 'move e1 e2 promote', 'move e1 e3', 'move e1 e3 promote', 'move e1 e4', 'move e1 e4 promote', 'move e1 e5', 'move e1 e5 promote', 'move e1 d2', 'move e1 d2 promote', 'move e1 c3', 'move e1 c3 promote', 'move e1 b4', 'move e1 b4 promote', 'move e1 a5', 'move e1 a5 promote', 'move e2 a2', 'move e2 b2', 'move e2 c2', 'move e2 d2', 'move e2 e1', 'move e2 e1 promote', 'move e2 e3', 'move e2 e4', 'move e2 e5', 'move e2 e5 promote', 'move e2 d3', 'move e2 c4', 'move e2 b5', 'move e2 b5 promote', 'move e2 d1', 'move e2 d1 promote', 'move e3 a3', 'move e3 b3', 'move e3 c3', 'move e3 d3', 'move e3 e1', 'move e3 e1 promote', 'move e3 e2', 'move e3 e4', 'move e3 e5', 'move e3 e5 promote', 'move e3 d4', 'move e3 c5', 'move e3 c5 promote', 'move e3 d2', 'move e3 c1', 'move e3 c1 promote', 'move e4 a4', 'move e4 b4', 'move e4 c4', 'move e4 d4', 'move e4 e1', 'move e4 e1 promote', 'move e4 e2', 'move e4 e3', 'move e4 e5', 'move e4 e5 promote', 'move e4 d5', 'move e4 d5 promote', 'move e4 d3', 'move e4 c2', 'move e4 b1', 'move e4 b1 promote', 'move e5 a5', 'move e5 a5 promote', 'move e5 b5', 'move e5 b5 promote', 'move e5 c5', 'move e5 c5 promote', 'move e5 d5', 'move e5 d5 promote', 'move e5 e1', 'move e5 e1 promote', 'move e5 e2', 'move e5 e2 promote', 'move e5 e3', 'move e5 e3 promote', 'move e5 e4', 'move e5 e4 promote', 'move e5 d4', 'move e5 d4 promote', 'move e5 c3', 'move e5 c3 promote', 'move e5 b2', 'move e5 b2 promote', 'move e5 a1', 'move e5 a1 promote', 'drop p a1', 'drop p a2', 'drop p a3', 'drop p a4', 'drop p b1', 'drop p b2', 'drop p b3', 'drop p b4', 'drop p c1', 'drop p c2', 'drop p c3', 'drop p c4', 'drop p d1', 'drop p d2', 'drop p d3', 'drop p d4', 'drop p e1', 'drop p e2', 'drop p e3', 'drop p e4', 'drop g a1', 'drop g a2', 'drop g a3', 'drop g a4', 'drop g a5', 'drop g b1', 'drop g b2', 'drop g b3', 'drop g b4', 'drop g b5', 'drop g c1', 'drop g c2', 'drop g c3', 'drop g c4', 'drop g c5', 'drop g d1', 'drop g d2', 'drop g d3', 'drop g d4', 'drop g d5', 'drop g e1', 'drop g e2', 'drop g e3', 'drop g e4', 'drop g e5', 'drop s a1', 'drop s a2', 'drop s a3', 'drop s a4', 'drop s a5', 'drop s b1', 'drop s b2', 'drop s b3', 'drop s b4', 'drop s b5', 'drop s c1', 'drop s c2', 'drop s c3', 'drop s c4', 'drop s c5', 'drop s d1', 'drop s d2', 'drop s d3', 'drop s d4', 'drop s d5', 'drop s e1', 'drop s e2', 'drop s e3', 'drop s e4', 'drop s e5', 'drop b a1', 'drop b a2', 'drop b a3', 'drop b a4', 'drop b a5', 'drop b b1', 'drop b b2', 'drop b b3', 'drop b b4', 'drop b b5', 'drop b c1', 'drop b c2', 'drop b c3', 'drop b c4', 'drop b c5', 'drop b d1', 'drop b d2', 'drop b d3', 'drop b d4', 'drop b d5', 'drop b e1', 'drop b e2', 'drop b e3', 'drop b e4', 'drop b e5', 'drop r a1', 'drop r a2', 'drop r a3', 'drop r a4', 'drop r a5', 'drop r b1', 'drop r b2', 'drop r b3', 'drop r b4', 'drop r b5', 'drop r c1', 'drop r c2', 'drop r c3', 'drop r c4', 'drop r c5', 'drop r d1', 'drop r d2', 'drop r d3', 'drop r d4', 'drop r d5', 'drop r e1', 'drop r e2', 'drop r e3', 'drop r e4', 'drop r e5', 'drop P a2', 'drop P a3', 'drop P a4', 'drop P a5', 'drop P b2', 'drop P b3', 'drop P b4', 'drop P b5', 'drop P c2', 'drop P c3', 'drop P c4', 'drop P c5', 'drop P d2', 'drop P d3', 'drop P d4', 'drop P d5', 'drop P e2', 'drop P e3', 'drop P e4', 'drop P e5', 'drop G a1', 'drop G a2', 'drop G a3', 'drop G a4', 'drop G a5', 'drop G b1', 'drop G b2', 'drop G b3', 'drop G b4', 'drop G b5', 'drop G c1', 'drop G c2', 'drop G c3', 'drop G c4', 'drop G c5', 'drop G d1', 'drop G d2', 'drop G d3', 'drop G d4', 'drop G d5', 'drop G e1', 'drop G e2', 'drop G e3', 'drop G e4', 'drop G e5', 'drop S a1', 'drop S a2', 'drop S a3', 'drop S a4', 'drop S a5', 'drop S b1', 'drop S b2', 'drop S b3', 'drop S b4', 'drop S b5', 'drop S c1', 'drop S c2', 'drop S c3', 'drop S c4', 'drop S c5', 'drop S d1', 'drop S d2', 'drop S d3', 'drop S d4', 'drop S d5', 'drop S e1', 'drop S e2', 'drop S e3', 'drop S e4', 'drop S e5', 'drop B a1', 'drop B a2', 'drop B a3', 'drop B a4', 'drop B a5', 'drop B b1', 'drop B b2', 'drop B b3', 'drop B b4', 'drop B b5', 'drop B c1', 'drop B c2', 'drop B c3', 'drop B c4', 'drop B c5', 'drop B d1', 'drop B d2', 'drop B d3', 'drop B d4', 'drop B d5', 'drop B e1', 'drop B e2', 'drop B e3', 'drop B e4', 'drop B e5', 'drop R a1', 'drop R a2', 'drop R a3', 'drop R a4', 'drop R a5', 'drop R b1', 'drop R b2', 'drop R b3', 'drop R b4', 'drop R b5', 'drop R c1', 'drop R c2', 'drop R c3', 'drop R c4', 'drop R c5', 'drop R d1', 'drop R d2', 'drop R d3', 'drop R d4', 'drop R d5', 'drop R e1', 'drop R e2', 'drop R e3', 'drop R e4', 'drop R e5']

class MiniShogiGame(Game):
    """
    This class specifies the base Game class. To define your own game, subclass
    this class and implement the functions below. This works when the game is
    two-player, adversarial and turn-based.

    Use 1 for player1 and -1 for player2.

    See othello/OthelloGame.py for an example implementation.
    """
    moveDict = {}           # For getValidMoves(), str_move -> idx_move.
    for i in range(len(all_valid_move)):
        moveDict[all_valid_move[i]] = i

    # Static variable for getCanonicalForm().
    __swap_normal_piece_idx = np.roll(np.arange(20), 10)

    def __init__(self):
        self.__Board_cache = None
        self.__id_nngb_o = -1   # Ordinary view on Board.
        self.__id_nngb_r = -1   # Rotated view on Board.

    def __Board_cache_handler(self, board: np.ndarray, player: int):
        # 144666 created / 910000 total. ~15.90%.
        id_board = id(board)
        if self.__id_nngb_o == id_board:
            self.__Board_cache.set_turn_player(player)
            return self.__Board_cache
        elif self.__id_nngb_r == id_board:
            self.__Board_cache.set_turn_player(player, to_rotate=True)
            # Swap ids in pair.
            self.__id_nngb_o, self.__id_nngb_r = self.__id_nngb_r, self.__id_nngb_o
            return self.__Board_cache

        b = Board(board, player)
        self.__Board_cache = b
        self.__id_nngb_o = id_board
        self.__id_nngb_r = -1
        return b

    def __Board_cache_updater(self, board: np.ndarray):
        self.__id_nngb_o = id(board)
        self.__id_nngb_r = -1

    @classmethod
    def getNNGameBoard(cls, str_or_obj_gameboard: np.ndarray, obj_player1: Player, obj_player2: Player):
        return Board.getNNGameBoard(str_or_obj_gameboard, obj_player1, obj_player2)

    @classmethod
    def getObjGameBoard(cls, nngameboard: np.ndarray):
        return Board.getObjGameBoard(nngameboard)

    @classmethod
    def getStrGameBoard(cls, objgameboard: np.ndarray):
        return Board.getStrGameBoard(objgameboard)

    @classmethod
    def getObjGameCapture(cls, nngameboard: np.ndarray, player: int) -> list:
        return Board.getObjGameCapture(nngameboard, player)

    def getInitBoard(self):
        """
        Returns:
            startBoard: a representation of the board (ideally this is the form
                        that will be the input to your neural network)
        """
        b = Board.init_board()
        board = b.nngameboard
        self.__Board_cache = b
        self.__Board_cache_updater(board)
        return board

    def getBoardSize(self):
        """
        Returns:
            (x,y,z): a tuple of board dimensions
        """
        return (5, 5, 40)

    def getActionSize(self):
        """
        Returns:
            actionSize: number of all possible actions
        """
        return 746

    @classmethod
    def _rotateActionMove(cls, str_move: str) -> str:
        return Board._rotateActionMove(str_move)

    def getNextState(self, board: np.ndarray, player: int, action: int):
        """

        Note: no matter a player is on which side, it always treats the player
              as player 1.
              hence, you can pick an action from getValidMoves() directly for
              player -1 without any conversion.

        Input:
            board: current board
            player: current player (1 or -1)
            action: action taken by current player

        Returns:
            nextBoard: board after applying action
            nextPlayer: player who plays in the next turn (should be -player)
        """
        b = self.__Board_cache_handler(board, player)

        str_move = all_valid_move[action]

        # print_func = logging.debug
        # display(b, print_func=print_func,
        #     str_prefix='Board:\n',
        #     str_suffix='\nCurrent player: %d \ngetNextState valid move: %s' % (player, str_move))

        if player == -1:
            # Actions are picked via canonical form in Coach and Arena.
            # Rotate -1 player's action move
            str_move = Board._rotateActionMove(str_move)
            # print_func("-1 player getNextState valid move: " + str_move)

        b.execute_move(str_move)

        # display(b, print_func=print_func,
        #     str_prefix='%d player moved board:\n' % player,
        #     str_suffix='\n%s' % ('-'*20))

        NNGameBoard = b.nngameboard
        self.__Board_cache_updater(NNGameBoard)
        return (NNGameBoard, -player)

    def getValidMoves(self, board: np.ndarray, player: int, depth: int = 0, round: int = 0) -> np.ndarray:
        """

        Note: no matter a player is on which side, it always treats the player
              as player 1.
              that is, the following return the same result.
              MiniShogiGame.getValidMoves(board, -1)
              MiniShogiGame.getValidMoves(MiniShogiGame.getCanonicalForm(board, -1), 1)

        Input:
            board: current board
            player: current player

        Returns:
            validMoves: a binary vector of length self.getActionSize(), 1 for
                        moves that are valid from the current board and player,
                        0 for invalid moves
        """

        valids = np.zeros(self.getActionSize(), dtype=np.int8)
        b = self.__Board_cache_handler(board, player)

        # Create valid move.
        # If can catch other team king, so prioritizing appended.
        legalMoves = []

        if not legalMoves:
            legalMoves = b.get_avoid_checkedOurKing_moves()

        # if not legalMoves:
        #     if depth == 0 and round > 10:
        #         legalMoves = b.get_check_otherPlayerPieces_moves()

        if not legalMoves:
            legalMoves = b.get_all_legal_moves()

        # Store valid move.
        if player == -1:
            for str_move in legalMoves:
                str_move = self._rotateActionMove(str_move)
                valids[ self.moveDict[str_move] ] = 1
        else:
            for str_move in legalMoves:
                valids[ self.moveDict[str_move] ] = 1

        return valids

    def getGameEnded(self, board: np.ndarray, player: int):
        """
        Input:
            board: current board
            player: current player (1 or -1)

        Returns:
            r: 0 if game has not ended. 1 if player won, -1 if player lost,
               small non-zero value for draw.

        """

        r = 0
        b = self.__Board_cache_handler(board, player)

        is_ourKing_inCheck, avoidCheckMoves, avoidCheckDrops = b.isInCheck()
        if is_ourKing_inCheck and len(avoidCheckMoves) + len(avoidCheckDrops) == 0:
            # 1. we have no any valid moves to make our King survive;
            # 2. our King has been captured by enemy.
            r = -1

        is_foeKing_inCheck, avoidCheckMoves, avoidCheckDrops = b.isInCheck(is_for_enemy=True)
        if is_foeKing_inCheck and len(avoidCheckMoves) + len(avoidCheckDrops) == 0:
            # 1. enemy has no any valid moves to make their King survive;
            # 2. their King has been captured by our team.
            r = 1

        return r

    def getCanonicalForm(self, board: np.ndarray, player: int):
        """
        Input:
            board: current board
            player: current player (1 or -1)

        Returns:
            canonicalBoard: returns canonical form of board. The canonical form
                            should be independent of player. For e.g. in chess,
                            the canonical form can be chosen to be from the pov
                            of white. When the player is white, we can return
                            board as is. When the player is black, we can invert
                            the colors and return the board.
        """
        if player == -1:
            # Record id of the original board which has not be rotated yet.
            id_board = id(board)

            # Associate -1 player with canonicalForm
            board = board.copy()
            # Rotate 2 times (180 degree, counterclockwise).
            # A rotated board and original one share the same space, thus side effect
            # exists.
            board = np.rot90(board, 2)
            # Switch owners of pieces (including captured).
            board *= -1
            # Swap sub-boards of two players.
            board[:, :, :20] = board[:, :, self.__swap_normal_piece_idx]

            # Record the new rotated board as the rotated view of the original one.
            if self.__id_nngb_o == id_board:
                self.__id_nngb_r = id(board)
            elif self.__id_nngb_r == id_board:
                self.__id_nngb_o = id(board)

        return board

    def stringRepresentation(self, board: np.ndarray):
        """
        Input:
            board: current board

        Returns:
            boardString: a quick conversion of board to a string format.
                         Required by MCTS for hashing.
        """
        return board.tostring()

    @classmethod
    def display(cls, board, *args, **kwargs):
        display(board, *args, **kwargs)


def display(board, player: int = 1, print_func=print, str_prefix: str = '', str_suffix: str = ''):
    """display current board via specified output function.

    Arguments:
        board {np.ndarray/Board} -- A current board. NNGameBoard, ObjGameBoard
                                    and an object of Board (including capture lists)
                                    are allowed.

    Keyword Arguments:
        player {int} -- current player (to obtain its captures) (default: {1})
        print_func {function} -- use it to output message (default: {print})
        str_prefix {str} -- a string prepend to message (default: {''})
        str_suffix {str} -- a string append to message (default: {''})

    Raises:
        Exception -- board is not an allowed type.
    """
    m = MiniShogiBoard()
    if type(board) is Board:
        strgameboard = board.strgameboard
        obj_player   = board.obj_player
        obj_enemy    = board.obj_enemy
    elif type(board) is not np.ndarray:
        raise Exception('board @ display() should be an object of either np.ndarray or Board.')
    elif board.dtype is np.dtype(object):
        # ObjGameBoard.
        strgameboard = Board.getStrGameBoard(board)
        obj_player   = None
        obj_enemy    = None
    else:
        # NNGameBoard.
        board = Board(board, player)
        strgameboard = board.strgameboard
        obj_player   = board.obj_player
        obj_enemy    = board.obj_enemy

    str_message = str_prefix + m.stringifyBoard(strgameboard)
    if obj_player:
        str_message += '\n' + "Our Captures: " + " ".join(obj_player.captured)
    if obj_enemy:
        str_message += '\n' + "Other Captures: " + " ".join(obj_enemy.captured)
    str_message += str_suffix

    print_func(str_message)
