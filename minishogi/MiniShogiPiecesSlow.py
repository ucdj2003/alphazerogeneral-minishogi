from abc import ABCMeta, abstractmethod

class Piece(object):
    __metaclass__ = ABCMeta

    def __init__(self, team, pieceType, position, direction, promoted):
        self.team = team
        self.pieceType = pieceType
        self.position = position
        self.promoted = promoted
        self.direction = direction
        self.opposite = self.setOpposite()

    def setOpposite(self):
        if self.team == 1:
            opposite = -1
        else:
            opposite = 1
        return opposite


class King(Piece):
    pass


class Rook(Piece):
    pass


class Bishop(Piece):
    pass


class GGeneral(Piece):
    pass


class SGeneral(Piece):
    pass


class Pawn(Piece):
    pass
