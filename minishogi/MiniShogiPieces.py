from abc import ABCMeta, abstractmethod

class Piece(object):
    __metaclass__ = ABCMeta

    __GGeneralPattern_up = [(0, 1), (1, 0), (0, -1), (-1, 0),
                            (1, 1), (-1, 1)]
    __GGeneralPattern_dn = [(0, 1), (1, 0), (0, -1), (-1, 0),
                            (1, -1), (-1, -1)]

    def __init__(self, team, position, promoted=False):
        self.team = team
        self.position = position
        self.promoted = promoted

    @property
    def _piece_code(self):
        if not hasattr(self, '__piece_code'):
            # self.__piece_code = self._pcode + ((self.promoted << 2) | (self.team & 10))
            self.__piece_code = self._pcode + (self.promoted * 4) + 5*(1 - self.team)       # Surprisingly FASTER!
        return self.__piece_code

    @abstractmethod
    def moves(self):
        pass

    @abstractmethod
    def promote(self):
        pass

    # make sure that the position is inside the boundaries of the game board
    def inBounds(self, position):
        x, y = position
        return x <= 4 and x >= 0 and y <= 4 and y >= 0

    # loops to check diagonals or cardinal directions in the case of a rook or bishop that can move like this
    def loopCheck(self, pattern, objgameboard):
        x, y = self.position
        opposite = -self.team
        possMoves = []
        for num in pattern:
            i = num[0] + x
            j = num[1] + y

            while i <= 4 and i >= 0 and j <= 4 and j >= 0:
                obj_piece = objgameboard[i, j]
                if not obj_piece:
                    possMoves.append((i, j))
                # if the square is held by the other team it's a possible move but must break the loop
                elif obj_piece.team == opposite:
                    possMoves.append((i, j))
                    break
                else:
                    break
                i += num[0]
                j += num[1]
        return possMoves

    # pattern provided to this function dictates the directions in which the piece may move
    def patternCheck(self, pattern, objgameboard):
        x, y = self.position
        opposite = -self.team
        possMoves = []
        for num in pattern:
            i = num[0] + x
            j = num[1] + y
            # can place onto any square that is in bounds and not containing a piece from the same team
            if i <= 4 and i >= 0 and j <= 4 and j >= 0:
                obj_piece = objgameboard[i, j]
                if not obj_piece or obj_piece.team == opposite:
                    possMoves.append((i, j))
        return possMoves

    def is_reachable(self, foe_piece_pos, objgameboard=None):
        if not self.promoted:
            pattern = self._pattern
        else:
            pattern = self._pattern_promoted
        dx = foe_piece_pos[0] - self.position[0]
        dy = foe_piece_pos[1] - self.position[1]
        return (dx, dy) in pattern

    def threatening_moves(self, dx, dy, objgameboard):
        """
        For King, GGeneral, SGeneral, Pawn.
        (dx, dy) is a shift vector originating from the self piece.
        """
        if not self.promoted:
            pattern = self._pattern
        else:
            pattern = self._pattern_promoted
        x, y = self.position
        x_beg, x_end = dx - 1, dx + 2
        y_beg, y_end = dy - 1, dy + 2
        x_beg = x_beg if x_beg > -1 else -1 # max(x_beg, -1)
        x_end = x_end if x_end < 2 else 2   # min(x_end, 2), +1 already.
        y_beg = y_beg if y_beg > -1 else -1 # max(y_beg, -1)
        y_end = y_end if y_end < 2 else 2   # min(y_end, 2), +1 already.
        t_moves = set()
        for i in range(x_beg, x_end):
            for j in range(y_beg, y_end):
                # (i, j) is in [-1, 1]x[-1, 1].
                if not (i or j):
                    # (0, 0), self piece.
                    continue
                if (i, j) not in pattern:
                    continue
                # Now, (i, j) is a possible threatening moving vector for self piece.
                nx, ny = x + i, y + j
                if dx == i and dy == j:
                    t_moves.add((nx, ny))
                elif nx <= 4 and nx >= 0 and ny <= 4 and ny >= 0:
                    obj_piece = objgameboard[nx, ny]
                    if not obj_piece:
                        # This position is reachable for foe_piece in this round
                        # and self piece in next round.
                        t_moves.add((nx, ny))
        return t_moves


class King(Piece):
    pieceType = 'King'
    _pcode = 0
    _kingPattern = [(0, 1), (1, 0), (0, -1), (-1, 0),
                    (1, 1), (-1, 1), (1, -1), (-1, -1)]

    def moves(self, objgameboard):
        moves = self.patternCheck(self._kingPattern, objgameboard)
        return moves

    def promote(self):
        raise Exception('Illegal move, King cannot be promoted')

    def is_reachable(self, foe_piece_pos, objgameboard=None):
        # A bit faster version than an inherited method.
        dx = foe_piece_pos[0] - self.position[0]
        dy = foe_piece_pos[1] - self.position[1]
        return (dx, dy) in self._kingPattern

    @property
    def _pattern(self):
        # It is compatible with threatening_moves() for convenience.
        # It is faster rather than examining if Piece.pieceType = 'King' before
        # calling King._kingPattern.
        return self._kingPattern


class Rook(Piece):
    pieceType = 'Rook'
    _pcode = 4
    __rookPattern = [(0, 1), (1, 0), (0, -1), (-1, 0)]      # Including promoted pattern.
    __kingPattern_ = [(1, 1), (-1, 1), (1, -1), (-1, -1)]

    def moves(self, objgameboard):
        moves = self.loopCheck(self.__rookPattern, objgameboard)
        if self.promoted:
            moves.extend(self.patternCheck(self.__kingPattern_, objgameboard))
        return moves

    def promote(self):
        self.promoted = True

    def is_reachable(self, foe_piece_pos, objgameboard):
        dx = foe_piece_pos[0] - self.position[0]
        dy = foe_piece_pos[1] - self.position[1]
        if self.promoted and (dx, dy) in self.__kingPattern_:
            return True
        if dx and dy:
            # One of dx and dy should be zero if reachable.
            return False
        n_grid = abs(dx or dy)
        dx, dy = dx//n_grid, dy//n_grid
        ray_moves = self.loopCheck([(dx, dy)], objgameboard)
        return foe_piece_pos in ray_moves

    def threatening_moves(self, foe_piece_pos, objgameboard):
        raise Exception('threatening_moves() is not for Rooks.')


class Bishop(Piece):
    pieceType = 'Bishop'
    _pcode = 3
    __bishopPattern = [(1, 1), (-1, 1), (1, -1), (-1, -1)]  # Including promoted pattern.
    __kingPattern_ = [(0, 1), (1, 0), (0, -1), (-1, 0)]

    def moves(self, objgameboard):
        moves = self.loopCheck(self.__bishopPattern, objgameboard)
        if self.promoted:
            moves.extend(self.patternCheck(self.__kingPattern_, objgameboard))
        return moves

    def promote(self):
        self.promoted = True

    def is_reachable(self, foe_piece_pos, objgameboard):
        dx = foe_piece_pos[0] - self.position[0]
        dy = foe_piece_pos[1] - self.position[1]
        if self.promoted and (dx, dy) in self.__kingPattern_:
            return True
        if abs(dx) != abs(dy):
            return False
        n_grid = abs(dx)
        dx, dy = dx//n_grid, dy//n_grid
        ray_moves = self.loopCheck([(dx, dy)], objgameboard)
        return foe_piece_pos in ray_moves

    def threatening_moves(self, foe_piece_pos, objgameboard):
        raise Exception('threatening_moves() is not for Bishops.')


class GGeneral(Piece):
    pieceType = 'GGeneral'
    _pcode = 1
    __GGeneralPattern_up = Piece._Piece__GGeneralPattern_up
    __GGeneralPattern_dn = Piece._Piece__GGeneralPattern_dn

    def moves(self, objgameboard):
        moves = self.patternCheck(self._pattern, objgameboard)
        return moves

    def promote(self):
        raise Exception('Illegal move, Golden General cannot be promoted')

    @property
    def _pattern(self):
        return self.__GGeneralPattern_up if self.team == 1 else self.__GGeneralPattern_dn


class SGeneral(Piece):
    pieceType = 'SGeneral'
    _pcode = 2
    __SGeneralPattern_up = [(1, 1), (-1, 1), (1, -1), (-1, -1),
                            (0, 1)]
    __SGeneralPattern_dn = [(1, 1), (-1, 1), (1, -1), (-1, -1),
                            (0, -1)]
    __GGeneralPattern_up = Piece._Piece__GGeneralPattern_up
    __GGeneralPattern_dn = Piece._Piece__GGeneralPattern_dn

    def moves(self, objgameboard):
        if not self.promoted:
            pattern = self._pattern
        else:
            pattern = self._pattern_promoted
        moves = self.patternCheck(pattern, objgameboard)
        return moves

    def promote(self):
        self.promoted = True

    @property
    def _pattern(self):
        return self.__SGeneralPattern_up if self.team == 1 else self.__SGeneralPattern_dn

    @property
    def _pattern_promoted(self):
        # GGeneral pattern.
        return self.__GGeneralPattern_up if self.team == 1 else self.__GGeneralPattern_dn


class Pawn(Piece):
    pieceType = 'Pawn'
    _pcode = 5
    __PawnPattern_up = [(0, 1)]
    __PawnPattern_dn = [(0, -1)]
    __GGeneralPattern_up = Piece._Piece__GGeneralPattern_up
    __GGeneralPattern_dn = Piece._Piece__GGeneralPattern_dn

    def moves(self, objgameboard):
        if not self.promoted:
            pattern = self._pattern
        else:
            pattern = self._pattern_promoted
        moves = self.patternCheck(pattern, objgameboard)
        return moves

    def promote(self):
        self.promoted = True

    @property
    def _pattern(self):
        return self.__PawnPattern_up if self.team == 1 else self.__PawnPattern_dn

    @property
    def _pattern_promoted(self):
        # GGeneral pattern.
        return self.__GGeneralPattern_up if self.team == 1 else self.__GGeneralPattern_dn
